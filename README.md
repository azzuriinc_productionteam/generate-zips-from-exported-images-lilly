# Generate zips from exported images

## このスクリプトの流れ：
1. imageOptimを使ってPhotoshopからExportされた画像を圧縮する
2. 画像をリリーのZIP名に合うように更新する
3. 指定のテンプレートを元にCSVを作る
4. 既存Generatorのmain.phpを呼ぶ
5. 作業後、ZIPがあるフォルダーを開く

## Dependencies
画像圧縮に使うimageOptimのCLIが必要です
[imageOptim CLI](https://github.com/JamieMason/ImageOptim-CLI)

Homebrewが既にインストールされてる方は:

```
brew update
brew install imageoptim-cli
```

とターミナルに打ち込めばOKです

## Setup
1. 本スクリプト、テーラーが書いた半端なスクリプトなので
実際にソースをユーザーごとに変える必要がございます。

`### CHANGE HERE`
と書いてあるところの**下**のディレクトリーやらパス等を変更してください

2. シェルの$PATHのうちのフォルダーにこのスクリプトを入れる。
自分は「mybin」っていうフォルダーをホームディレクトリーに設けて、そこをシェルの＄PATHに付け足してます。

3. 念のため、`chmod +x generator`を実行しおくのもいいかもしれません。

## 使い方
pdfをphotoshopで開く前に、ファイル名をZIP名の出だしの００までに設定します。以下例。 

`今回スライドにする資料.pdf` => `ONC-GI-JP-JA-CRCSLIDE-08_00`

通常通りフォトショでエクスポートすると、こんな感じでエクスポートされます↓
```
ONC-GI-JP-JA-CRCSLIDE-08_00-1
ONC-GI-JP-JA-CRCSLIDE-08_00-2
...
```
ファイル名はこのままで大丈夫です！
後はターミナルに`generator`と打って実行するのみです。
画像の圧縮、名前変更、フォルダー移行が済んだら
テンプレを選んでくださいというのが出て来ます。
以上です！


## 注意事項
作業後`generator/images`や`generator/results`の内容はクリアされません
俺の勧める方法は、実行ごとに作成されたフォルダー、ジップ、画像を別のフォルダーに移してから、generatorの内容を消す事です。
